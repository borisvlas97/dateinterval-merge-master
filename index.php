<?php

/**
 * @param $input
 * @return mixed
 * @todo implement function
 */
function merge($input)
{
    $result = [];

    if (is_array($input)) {

        $range_array = [];

        foreach ($input as $range) {
            if (count($range) === 2) {
                $add_new = true;

                $start_date_time = explode(' ', $range[0]);
                $end_date_time = explode(' ', $range[1]);

                $start_date = $start_date_time[0];
                $start_time = $start_date_time[1];

                $end_date = $end_date_time[0];
                $end_time = $end_date_time[1];

                if (!empty($range_array)) {
                    foreach ($range_array as &$exist_range) {
                        if (($start_date > $exist_range['start']['date'] && $start_date < $exist_range['end']['date']) ||
                            ($end_date > $exist_range['end']['date'] && $end_date < $exist_range['end']['date'])) {

                            $exist_range['start']['date'] = min($start_date, $exist_range['start']['date']);
                            $exist_range['end']['date'] = max($end_date, $exist_range['end']['date']);

                            $exist_range['start']['time'] = min($start_time, $exist_range['start']['time']);
                            $exist_range['end']['time'] = max($end_time, $exist_range['end']['time']);

                            $add_new = false;
                            break;
                        } else {
                            if ($start_date === $exist_range['start']['date'] && $end_date === $exist_range['end']['date']) {
                                if (($start_time > $exist_range['start']['time'] && $start_time < $exist_range['end']['time']) ||
                                    ($end_time > $exist_range['start']['time'] && $end_time < $exist_range['end']['time'])) {

                                    $exist_range['start']['time'] = min($start_time, $exist_range['start']['time']);
                                    $exist_range['end']['time'] = max($end_time, $exist_range['end']['time']);

                                    $add_new = false;
                                    break;
                                }
                            } else {
                                if ($start_date === $exist_range['start']['date']) {
                                    if ($start_time > $exist_range['start']['time']) {

                                        $exist_range['end']['time'] = max($end_time, $exist_range['end']['time']);
                                        $exist_range['end']['date'] = max($end_date, $exist_range['end']['date']);

                                        $add_new = false;
                                        break;
                                    }
                                }
                                if ($end_date === $exist_range['end']['date']) {
                                    if ($end_time < $exist_range['end']['time']) {
                                        $exist_range['start']['time'] = min($start_time, $exist_range['start']['time']);
                                        $exist_range['start']['date'] = min($start_date, $exist_range['start']['date']);

                                        $add_new = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                if ($add_new) {
                    $range_array []= [
                        'start' => [
                            'date' => $start_date,
                            'time' => $start_time
                        ],
                        'end' => [
                            'date' => $end_date,
                            'time' => $end_time
                        ]
                    ];
                }
            }
        }

        foreach ($range_array as $range_date_time) {
            $start = $range_date_time['start']['date'] . ' ' . $range_date_time['start']['time'];
            $end = $range_date_time['end']['date'] . ' ' . $range_date_time['end']['time'];

            $result []= [$start, $end];
        }
    }

    return $result;
}